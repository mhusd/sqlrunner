@{
    # required
    # A Unique ID for the Task
    # new-guid is good for this.
    TaskID         = ''

    # optional
    # describe the task
    # will be used in logs
    Description    = ""

    # required
    # the sql script to run
    Script         = 'Path\To\Script.sql'
   
    # required
    # the database settings.
    # this can either be a path to a psd1 file or a hashtable
    # Database = @{Server='';Database='';Username='';Password=''}
    Database       = 'Path\to\psd1'
    
    # optional
    # discord logging.
    # this can either be a path to a psd1 file or a hashtable
    # Webhook = @{URI='';Name='';Channel=''}
    Webhook        = 'Path\to\psd1'

    # optional
    # use this if you would like returned data to be save to csv.
    # by default 'Invoke-SqlTask' returns data to the caller.
    Export         = 'Path\To\Outfile.csv'

    # optional 
    # receive email error notices
    # this can either be a path to a psd1 file or a hashtable
    # SMTP = @{ To = @(); From = ''; Bcc = '';Server = ''; Ssl = $true}
    SMTP = 'path\to\psd1'

    # optional
    # automatic variables {{logid}}, {{taskid}}, {{time}}
    # valid extentions are '.json' and '.yaml'
    Log            = '.\Logs\{{logid}}.yaml'

    # optional
    # output to console
    Debug          = $False
}