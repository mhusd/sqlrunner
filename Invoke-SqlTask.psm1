using module '.\Modules\Settings.psm1'
using module '.\Modules\Log.psm1'
using module '.\Modules\Export-DataRow.psm1'

function Invoke-SqlTask {
    param (
        [alias('Settings')]
        [system.io.fileinfo]$Path
    )
    try {
        $Settings = [Settings]::new($Path)
    
    }
    catch {
        throw "Invalid Settings. $psitem"
    }


    try {
        $Log = [log]::new(@{
                LogID       = New-Guid
                Date        = Get-date
                TaskID      = $Settings.TaskID.ToString()
                Description = $Settings.Description
                Script      = $Settings.Script.FullName
                Success     = $false
                Errors      = @()
            })

        $SqlCmdArgs = $Settings.SqlCmd()
        
        # write path to file being run.
        if ($Settings.Debug) {
            Write-Host "running $($Settings.Script.FullName)" -f Yellow
        }

        try {
            $data = Invoke-Sqlcmd @SqlCmdArgs -ErrorAction 'stop' -QueryTimeout 65535 -ConnectionTimeout 0   
            $Log.Success = $true

            # write successful script to console.
            if ($Settings.Debug) {
                try {
                    [string]$sql = Get-Content -Raw -Path $Settings.Script.FullName
                    Write-Host $sql -f green
                }
                catch { }
            }
        }
        catch {

            # write failes script to console.
            if ($Settings.Debug) {
                try {
                    [string]$sql = Get-Content -Raw -Path $Settings.Script.FullName
                    Write-Host $sql -f red
                }
                catch { }
            }

            throw $psitem
        }

        # export or return data
        if ($Settings.Export) {
            Export-DataRow -InputObject $data -Path $Settings.Export
        }
        else {
            return $Data
        }
    }
    catch {
        $Log.Errors += $PSItem.Exception.Message
    }

    #log
    finally {
        if ($Settings.Webhook) {
            Send-DiscordLog -Webhook $Settings.WebHook.URI -InputObject $Log
        }

        if ($Settings.smtp) {
            Send-EmailLog -InputObject $Log -SMTP $Settings.smtp
        }

        if ($Log.Errors -and $Settings.Debug) {
            Write-Host $($Log.Errors -join "`r`n") -f Red
        }

        Write-Log -InputObject $Log -Path $Settings.Log
        
    }
}
