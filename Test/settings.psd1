@{
    # required
    # A Unique ID for the Task
    # new-guid is good for this.
    TaskID         = '63cff0c5-75f1-444f-a61f-ae88b63fb31b'

    # optional
    # describe the task
    # will be used in logs
    Description    = 'this is a integreation test'

    # required
    # the sql script to run
    Script         = '.\test.sql'
   
    # required
    # the database settings.
    # this can either be a path to a psd1 file or a hashtable
    # Database = @{Server='';Database='';Username='';Password=''}
    Database       = 'C:\ProgramData\aeries.database.psd1'
    
    # optional
    # discord logging.
    # this can either be a path to a psd1 file or a hashtable
    # Webhook = @{URI='';Name='';Channel=''}
    Webhook        = 'C:\ProgramData\mhusd.webhook.logs.psd1'

    # optional
    # use this if you would like returned data to be save to csv.
    # by default 'Invoke-SqlTask' returns data to the caller.
    #Export         = 'Path\To\Outfile.csv'

    # optional 
    # receive email notices
    # this can either be a path to a psd1 file or a hashtable
    # SMTP = @{ To = @(); From = ''; Bcc = '';Server = ''; Ssl = $true}
    SMTP = 'C:\ProgramData\mhusd.smtp.logs.psd1'

    # optional
    # log file will be replaced every run
    # automatic variables {{logid}}, {{taskid}}, {{time}}
    # valid extentions are '.json' and '.yaml'
    Log            = '.\Logs\{{logid}}.yaml'

    # optional
    # output to console
    Debug          = $false
}