using module '.\powershell-yaml\powershell-yaml.psm1'
using module '.\ConvertTo-Hashtable.psm1'

function Export-DataRow {
    param (
        [parameter(ValueFromPipeline)]
        [system.data.datarow[]]$InputObject,

        [system.io.fileinfo]$Path
    )

    [string]$Data = ''
    switch ($Path.Extension) {
        '.json' {
            [array]$HT = $InputObject.foreach{ConvertTo-Hashtable -InputObject $psitem}
            [string]$Data = ConvertTo-json -InputObject $HT -ErrorAction 'stop'
        }
        '.yaml' {
            [array]$HT = $InputObject.foreach{ConvertTo-Hashtable -InputObject $psitem}
            [string]$Data = ConvertTo-Yaml -Data $HT
        }
        
        # CSV
        default {
            [array]$Data = $InputObject | ConvertTo-Csv -NoTypeInformation
            [string]$Data = $Data -join "`r`n"
        }
    }

    try {
        Out-File -InputObject $Data -FilePath $Path -Force -ErrorAction 'stop' | Out-Null
    }
    catch {
        throw $PSItem
    }
}