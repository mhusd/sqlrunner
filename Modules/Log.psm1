using module '.\powershell-yaml\powershell-yaml.psm1'
using module '.\Send-Discord.psm1'
using module '.\SMTP.psm1'


class Log {
    Log([System.Collections.IDictionary]$InputObject) {
        $this.LogID = $InputObject.LogID
        $this.Date = $InputObject.Date
        $this.TaskID = $InputObject.TaskID
        $this.Description = $InputObject.Description
        $this.Script = $InputObject.Script
        $this.Success = $InputObject.Success
        $this.Errors = $InputObject.Errors
    }
    [guid]$LogID
    [datetime]$Date
    [guid]$TaskID
    [string]$Description
    [string]$Script
    [bool]$Success
    [string[]]$Errors
}

function Send-DiscordLog {
    param(
        [parameter(Mandatory, ValueFromPipeline)]
        [Log]$InputObject,

        [system.uri]$Webhook
    )
    [string]$Log = $InputObject | ConvertTo-Yaml

    if ($InputObject.Success) {
        $Emoji = ':confetti_ball:'
    }
    else {
        $Emoji = ':boom:'
    }

    [string]$Message = "$Emoji`r`n``````yaml`r`n$($Log[0..1900] -join '')`r`n``````" 
    Send-Discord -Webhook $Webhook -Message $Message -sleep 3
}


function Write-Log {
    param (
        [system.io.fileInfo]$Path,
        [Log]$InputObject
    )

    # automatic variables
    if($Path.Name -cmatch '{{logid}}'){
        [system.io.fileInfo]$Path = $Path.FullName -replace '{{logid}}', $InputObject.LogID
    }
    
    if($Path.Name -cmatch '{{taskid}}'){
        [system.io.fileInfo]$Path = $Path.FullName -replace '{{taskid}}', $InputObject.LogID
    }
    if($Path.Name -cmatch '{{time}}'){
        [system.io.fileInfo]$Path = $Path.FullName -replace '{{time}}', $(get-date $InputObject.Date -Format 'FileDateTime')
    }

    # automatic path
    try {
        [bool]$PathExists = Test-Path $Path.Directory.FullName -ErrorAction 'stop'
    }
    catch {
        throw $psitem
    }
    if(!$PathExists){
        try {
            New-Item $Path.Directory.FullName -ErrorAction 'stop' -Force -ItemType 'Directory' | Out-Null
        }
        catch {
            throw $psitem
        }
    }


    [string]$Data = ''
    switch ($Path.Extension) {
        '.json' {
            [string]$Data = $InputObject | ConvertTo-Json
        }
        '.yaml' {
            [string]$Data = $InputObject | ConvertTo-Yaml
        }
        default {
            [string]$Data = $InputObject | ConvertTo-Json
        }
    }

    try {
        Out-File -InputObject $Data -FilePath $Path -Force -ErrorAction 'stop' | Out-Null
    }
    catch {
        throw $psitem
    }
}



function Send-EmailLog {
    param(
        [parameter(Mandatory, ValueFromPipeline)]
        [Log]$InputObject,
        
        [parameter(Mandatory)]
        [smtp]$smtp
    )
    [string]$body = "
    <div style='font-family:monospace;font-size:1.5em;'>
        <h1 style='color:blue'>Log</h1>
        <ul style='list-style-type:none;'>
            <li><b>LogID:</b> $($InputObject.LogID) </li>
            <li><b>Date:</b> $($InputObject.Date) </li>
            <li><b>TaskID:</b> $($InputObject.TaskID) </li>
            <li><b>Description:</b> $($InputObject.Description) </li>
            <li><b>Script:</b> $($InputObject.Script) </li>
            <li><b>Success:</b> $($InputObject.Success) </li>
            <li><b>Errors:</b> <ul style='list-style-type:square;color:red;'>
                    $(
                        foreach($e in $InputObject.Errors ){
                            "<li>$e</li>"
                        }
                    )
                </ul>
            </li>
        </ul>
    </div>
    "

    $SendArgs = @{
        Body        = $body
        To          = $smtp.To
        From        = $smtp.From
        SmtpServer  = $smtp.Server
        Subject     = $InputObject.LogID.ToString() 
        Priority    = 'High'
        UseSsl      = $true
        BodyAsHtml  = $true
        ErrorAction = 'stop'
    }
    if($smtp.Bcc){
        $SendArgs.Add('Bcc',$smtp.Bcc)
    }
    if($smtp.Cc){
        $SendArgs.Add('Cc',$smtp.Cc)
    }

    try {
        Send-MailMessage @SendArgs
    }
    catch {
        throw $psitem
    }
}