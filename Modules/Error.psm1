
function Send-EmailError {
    param(
        [parameter(Mandatory, ValueFromPipeline)]
        $ErrorObject,
        
        [parameter(Mandatory)]
        [string[]]$To,
        
        [string]$Subject,

        [Guid]$Guid = $(new-guid),
        [datetime]$Date = $(get-date),

        [string]$Comment,

        [system.io.fileinfo[]]$Attachments
    )

    [system.io.fileinfo]$File = $ErrorObject.InvocationInfo.ScriptName
    if (!$Subject) {
        $Subject = $File.Name + " [" + $guid + "]"
    }
    [string]$body = "
    <div style='font-family:monospace;font-size:1.5em;'>
        <h1>Error</h1>
        <ul style='list-style-type:none;'>
            <li>Date: $date </li>
            <li>Guid: $guid </li>
            <li>Path: $File </li>
            <li>Line: $($ErrorObject.InvocationInfo.ScriptLineNumber)</li>
            <li>Char: $($ErrorObject.InvocationInfo.OffsetInLine)</li>
        </ul>

        <h1>Message</h1>
        <p style='color:red; font-weight:bold;'>
        $($ErrorObject.Exception.Message)
        </p>
        <pre style='
            overflow:scroll;
            padding-left:5px;
            padding-right:5px;
            padding-top:10px;
            padding-bottom:10px;
            background-color:#fffedd;
            '>$($ErrorObject.InvocationInfo.line.trim())</pre>
        <!--Comment-->
    </div>
    "
    if ($Comment) {
        [string]$Commenthtml = "
        <h1>Comment</h1>
        <pre style='
            overflow:scroll;
            padding-left:5px;
            padding-right:5px;
            padding-top:10px;
            padding-bottom:10px;
            color:#7c7c7c;
            '>$Comment</pre>
        "
        $body = $body -replace "<!--Comment-->", $Commenthtml
    }
    $SendParam = @{ }
    $SendParam += @{
        Body        = $body
        To          = $To
        Bcc         = 'spam@mhusd.org'
        From        = 'noreply@mhusd.org'
        SmtpServer  = 'smtp-relay.gmail.com'
        Subject     = $Subject
        Priority    = 'High'
        UseSsl      = $true
        BodyAsHtml  = $true
        ErrorAction = 'stop'
    }
    if ($Attachments) {
        $SendParam.Attachments = $Attachments
    }

    try{
        Send-MailMessage @SendParam
    }
    catch{
        throw $psitem
    }
}