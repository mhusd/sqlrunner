FUNCTION Send-Discord {
    param(

        [parameter(Mandatory, ValueFromPipeline)]
        [string]
        $Message,

        [parameter(Mandatory)]
        [System.Uri]
        $Webhook,

        [int]
        $sleep
    )
    
    # discord doesnt like being spamed
    if ($sleep) {
        Start-Sleep -Seconds $sleep
    }

    $rest_param = @{
        Method      = 'POST'
        Uri         = $Webhook
        Body        = @{
            content = $Message
        }
        ErrorAction = 'stop'
    }
    try {
        $Response = Invoke-WebRequest @rest_param 
    }
    catch {
        throw $PSItem
    }
}