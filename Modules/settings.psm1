using module '.\smtp.psm1'


function Get-Path {
    param (
        [System.IO.DirectoryInfo]$Path,
        [string]$ChildPath
    )
    #dynamic paths
    if ($ChildPath -match '^[.][\/\\].*') {
        [int]$len = $ChildPath.length - 1
        [string]$Clean = $ChildPath[2..$len] -join ''
        [system.io.fileinfo]$NewPath = Join-Path -Path $Path.FullName -ChildPath $Clean
    }
    elseif ($ChildPath -match '^[.][.][\/\\].*') {
        [int]$len = $ChildPath.length - 1
        [string]$Clean = $ChildPath[3..$len] -join ''
        [system.io.fileinfo]$NewPath = Join-Path -Path $Path.Directory.FullName -ChildPath $Clean
    }

    # literal paths
    else {
        [system.io.fileinfo]$NewPath = $ChildPath
    }

    return $NewPath.FullName
}


class Settings {
    Settings([system.io.fileinfo]$Path) {
        try {
            $Data = Import-PowerShellDataFile -Path $Path -ErrorAction 'stop'
        }
        catch {
            throw $PSItem
        }
        $this.ParseIDictionary($Data, $Path.Directory.FullName)
    }
    ParseIDictionary([system.collections.IDictionary]$InputObject, [system.io.directoryinfo]$WorkingDirectory) {
        try {
            $InputProperties = $InputObject.Keys
            if ($InputProperties -contains 'Database') {
                if ($InputObject.Database -isnot [system.collections.IDictionary]) {
                    [string]$DBPath = Get-Path -Path $WorkingDirectory -ChildPath $InputObject.Database
                    # assume a valid path was passed instead. 
                    try {
                        $InputObject.Database = Import-PowerShellDataFile -Path $DBPath -ErrorAction 'stop'
                    }
                    catch {
                        throw $psitem
                    }
                }

                $DBProperties = $InputObject.Database.Keys

                # Database Server
                if ($DBProperties -contains 'Server') {
                    $this.Server = $InputObject.Database.Server
                }
                else {
                    throw "Database Server was left blank."
                }

                # Database Name
                if ($DBProperties -contains 'Database') {
                    $this.Database = $InputObject.Database.Database
                }
                else {
                    throw "Database name was left blank."
                }

                # Database Credential
                if ($DBProperties -contains 'Username') {
                    if ($DBProperties -contains 'Password') {
                        $SecureString = ConvertTo-SecureString $InputObject.Database.Password -AsPlainText -Force -ErrorAction 'stop'
                        $this.Credential = [PSCredential]::new($InputObject.Database.Username, $SecureString)
                    }
                    else {
                        throw 'Password setting was left blank.'
                    }
                }
            }
            else {
                throw 'Database setting was left blank.'
            }

            if ($InputProperties -contains 'Script') {
                $this.Script =  Get-Path -Path $WorkingDirectory -ChildPath $InputObject.Script 
            }
            else {
                throw 'Script setting was left blank.'
            }

            if ($InputProperties -contains 'SMTP') {
                if ($InputObject.SMTP -isnot [system.collections.IDictionary]) {
                    [string]$SMTPPath = [string]$DBPath = Get-Path -Path $WorkingDirectory -ChildPath $InputObject.SMTP
                    $InputObject.SMTP = Import-PowerShellDataFile $SMTPPath -ErrorAction 'stop'
                }
                $this.SMTP = $InputObject.SMTP
            }
        
            if ($InputProperties -contains 'WebHook') {
                if ($InputObject.WebHook -isnot [system.collections.IDictionary]) {
                    [string]$WebHookPath = Get-Path -Path $WorkingDirectory -ChildPath $InputObject.WebHook
                    $InputObject.WebHook = Import-PowerShellDataFile $WebHookPath -ErrorAction 'stop'
                }
                $this.WebHook = $InputObject.WebHook
            }
        
            if ($InputProperties -contains 'Debug' -and $InputObject.Debug -is [bool]) {
                $this.Debug = $InputObject.Debug 
            }

            if ($InputProperties -contains 'Log') {
                $this.Log = Get-Path -Path $WorkingDirectory -ChildPath $InputObject.Log
            }

            if ($InputProperties -contains 'TaskID') {
                $this.TaskID = $InputObject.TaskID.ToString() 
            }

            if ($InputProperties -contains 'Description') {
                $this.Description = $InputObject.Description.ToString() 
            }

            if ($InputProperties -contains 'Export') {
                $this.Export = Get-Path -Path $WorkingDirectory -ChildPath $InputObject.Export 
            }
        }
        catch {
            throw $psitem
        }
    }

    [string]$TaskID
    [string]$Description
    [string]$Server
    [string]$Database
    [pscredential]$Credential
    [system.io.fileinfo]$Script
    [system.io.fileinfo]$Export
    [smtp]$SMTP
    [hashtable]$WebHook
    [system.io.fileinfo]$Log
    [bool]$Debug


    [hashtable] SqlCmd() {
        $Arguments = @{
            ServerInstance = $this.Server
            Database       = $this.Database
            InputFile      = $this.Script
        }
        if ($this.Credential) {
            $Arguments.add('Credential', $this.Credential)
        }
        return $Arguments
    }
}