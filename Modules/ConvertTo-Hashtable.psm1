<#
    ConvertTo-Hashtable converts key/value objects into hashtables

    [example 1]
    $table = [System.Data.DataTable]::new()
    $table.Columns.Add('a','int') | out-null
    $table.Columns.Add('b','int') | out-null
    $table.Columns.Add('c','int') | out-null
    $row = $table.NewRow()
    $row['a'] = 1
    $row['b'] = 2
    $row['c'] = 3

    ConvertTo-Hashtable $row -include @('a','c')  
    # returns @{a=1;c=3}

    [example 2]
    ConvertTo-Hashtable @{a=1;b=2;c=3} -include @('a','c')  
    # returns @{a=1;c=3}
#>
function ConvertTo-Hashtable {
    param (
        [parameter(ValueFromPipeline)]
        [psobject]$InputObject,
        [string[]]$Include,
        [string[]]$Exclude
    )
    [hashtable]$OutputObject = @{}
    # Datarow
    if ($InputObject -is [System.Data.Datarow]) {
        foreach ($Column in $InputObject.Table.Columns) {
            $Value = $InputObject[$Column]
            $Key = $Column.ColumnName
            if ($Include -and $Include -NotContains $Key) {
                continue
            }
            if ($Exclude -and $Exclude -Contains $Key) {
                continue
            }
            $OutputObject.Add($Key, $Value)
        }
        return $OutputObject
    }
    # Hashtable
    # creates a new hashtable with filtered keys,values
    if ($InputObject -is [System.Collections.IDictionary]) {
        foreach ($Key in $InputObject.Keys) {
            $Value = $InputObject[$Key]
            if ($Include -and $Include -NotContains $Key) {
                continue
            }
            if ($Exclude -and $Exclude -Contains $Key) {
                continue
            }
            $OutputObject.Add($Key, $Value)
        }
        return $OutputObject
    }
}