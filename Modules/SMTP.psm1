class smtp {
    smtp (){}
    smtp ([system.collections.IDictionary]$InputObject) {
        if(!$InputObject.Server -or !$InputObject.To -or !$InputObject.From){
            throw 'Invalid smtp Settings'
        }
        $this.To = $InputObject.To
        $this.From = $InputObject.From
        $this.Server = $InputObject.Server
        
        if($InputObject.Ssl){
            $this.Ssl = $InputObject.Ssl
        }
        if($InputObject.Bcc){
            $this.Bcc = $InputObject.Bcc
        }
        if($InputObject.Cc){
            $this.Cc = $InputObject.Cc
        }
    }
    [string[]]$To
    [string[]]$Bcc
    [string[]]$Cc
    [string]$From
    [string]$Server
    [bool]$Ssl
}